# Adressenboek

De relevante bestanden zijn te vinden in de volgende mappen:

HTML: resources/views/home.blade.php

JS en SASS source files: public/src

Backend code: app/http/controllers/ApiController.php

Database dump zit in databases.sql

Database gegevens moeten aangepast worden in de file genaamd ".env" welke in de root staat.
